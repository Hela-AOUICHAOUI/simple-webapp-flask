import os
from flask import Flask
app = Flask(__name__)

@app.route("/")
def main():
    return "Welcome to the training on LPI DevOps Tools Engineer at The Way Center!"

@app.route('/about')
def hellowelcome():
    return 'Welcome to the training on lpi 701-100 Exam'
	
@app.route('/org')
def helloorg():
    return 'THE WAY CENTER'

@app.route('/amine')
def helloamine():
    return 'Welcome Mohamed Amine! You have successfully deploy this container'
@app.route('/wissal')
def hellowissal():
    return 'Welcome Wissal Boudabbous! You have successfully deploy this container'

@app.route('/mouna')
def hellomouna():
    return 'Welcome Mouna Ben Mansour! You have successfully deploy this container'
	
@app.route('/hela')
def hellohela():
    return 'Welcome Hela Aouichaoui! You have successfully deploy this container'

@app.route('/dhouha')
def hellodhouha():
    return 'Welcome Dhouha Bennaceur! You have successfully deploy this container'



if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
